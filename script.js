/**
 * Decode function is the main entry point in the script,
 * and you should not delete it.
 *
 * Arguments:
 * 'payload' - an array of bytes, raw payload from sensor
 * 'port' - an integer value in range [0, 255], f_port value from sensor
 */
 
var hexPayload = '';

function reverseString(str) {
    return str.match(/.{1,2}/g).reverse().join("");
}

function decode(payload, port) {
    var result = new Object();
    Object.keys(payload).forEach(function(key) {
        thishex = Number(payload[key]).toString(16);
        thishex = thishex.length < 2 ? "" + "0" + thishex : thishex;
        hexPayload += thishex;
    });
    result.RawPayload = hexPayload;

    /**
	 *payload here is referred to as MeterReadingMessage
     *consists of three parts (in hexadecimal) meterReadingMessageHeader, meterID(Integer), SEQUENCE
     *meterReadingMessageHeader = {Version, Medium, Qualifier of Medium}
     *meterID = {bytes in reversed order}
     *SEQUENCE/RegisterValue = {measured value(bytes) in reversed order}
	 **/

    var header = '0' + (parseInt(hexPayload.substring(0, 2), 16).toString(2)); /*.padStart(8,'0');*/
    /*header = header.prototype.padStart(8,'0');*/
    result.version = parseInt(header.substring(0, 2), 2).toString(10);
    var medium = ['HeatCostAllocater', 'Temperature', 'Electricity', 'Gas', 'Heat', '', 'HotWater', 'Water'];
    for (i = (parseInt(header.substring(2, 5), 2).toString(10)); i < medium.length;) {
        result.medium = medium[i];
        console.log(medium[i]);
        break;
    }

    var electricity = ['None', 'A_Plus', 'A_Plus_T1_T2', '', 'A_Plus_A_Minus', 'A_Minus', 'A_Plus_T1_T2_A_Minus']
    for (i = (parseInt(header.slice(5), 2).toString(10)); i < electricity.length;) {
        result.qualifierElectricity = electricity[i];
        break;
    }
    result.meterID = parseInt((reverseString(hexPayload.substring(2, 10))), 16).toString(10);
    var revValue = reverseString(hexPayload.substring(10));
    result.Value = parseInt(revValue.slice(6), 16).toString(10) / 100 + 'kWh';
    return {
        "result": result,
        "port": port
    }
}
/*#TODO#
#Implement padStart() at line 32/33 or check why padStart() is not working in node.js
#Implement functions for selecting medium and qualifiers
*/