/**
 * Decode function is the main entry point in the script,
 * and you should not delete it.
 *
 * Arguments:
 * 'payload' - an array of bytes, raw payload from sensor
 * 'port' - an integer value in range [0, 255], f_port value from sensor
 */
var hexPayload = '';

function reverseString(str) {
    return str.match(/.{1,2}/g).reverse().join("");
}

function decode(payload, port) {
    var result = new Object();
    Object.keys(payload).forEach(function(key) {
        thishex = Number(payload[key]).toString(16);
        thishex = thishex.length < 2 ? "" + "0" + thishex : thishex;
        hexPayload += thishex;
    });


    /**
     *payload here is referred to as MeterReadingMessage
     *consists of three parts (in hexadecimal) meterReadingMessageHeader, meterID(Integer), SEQUENCE
     *meterReadingMessageHeader = {Version, Medium, Qualifier of Medium}
     *meterID = {bytes in reversed order}
     *SEQUENCE/RegisterValue = {measured value(bytes) in reversed order}
     **/
    if (hexPayload.length == 38) {
        result.RawPayload = hexPayload;
        result.Port = port;
        var header = parseInt(reverseString(hexPayload.substring(0, 6)), 16).toString(2)
        //console.log(header);
        //var header = '0' + (parseInt(hexPayload.substring(0, 2), 16).toString(2)); /*.padStart(8,'0');*/
        /*header = header.prototype.padStart(8,'0');*/

        result.version = parseInt(header.substring(0, 2), 2).toString(10);
        var medium = ['HeatCostAllocater', 'Temperature', 'Electricity', 'Gas', 'Heat', '', 'HotWater', 'Water'];
        for (i = (parseInt(header.substring(4, 8), 2).toString(10)); i < medium.length;) {
            result.medium = medium[i];
            //console.log(medium[i]);
            break;
        }

        var electricity = ['None', 'A_Plus', 'A_Plus_T1_T2', '', 'A_Plus_A_Minus', 'A_Minus', 'A_Plus_T1_T2_A_Minus']
        for (i = (parseInt(header.substring(12, 16), 2).toString(10)); i < electricity.length;) {
            result.qualifierElectricity = electricity[i];
            break;
        }
        result.meterID = parseInt((reverseString(hexPayload.substring(6, 14))), 16).toString(10);
        var unixStampDecimal = (parseInt(reverseString(hexPayload.substring(14, 22)), 16).toString(10)) * 1000;
        //console.log(unixStampDecimal);
        var timeStamp = new Date(unixStampDecimal)
        result.Time = String(timeStamp);
        result.Value1 = parseInt(reverseString(hexPayload.substring(22, 30)), 16).toString(10) / 100;
        result.Value2 = parseInt(reverseString(hexPayload.substring(30)), 16).toString(10) / 100;
        result.Unit = 'kWh';
    } else {
        result.Payload = 'Invalid Payload';
    }
    return result;
}
/*#TODO#
#Implement padStart() at line 32/33 or check why padStart() is not working in node.js
#Implement functions for selecting medium and qualifiers
*/
